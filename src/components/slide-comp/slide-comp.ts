import { Component, ViewChild , Input, Output, EventEmitter} from '@angular/core';
import { Slides } from 'ionic-angular';

/*
  Generated class for the SlideComp component.

  See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
  for more info on Angular 2 Components.
*/
@Component({
  selector: 'slide-comp',
  templateUrl: 'slide-comp.html'
})

export class SlideComp {

  @Input() indice: string;
  @Input() images;
  @Output() update = new EventEmitter();
  @ViewChild("test") sliderComponent: Slides;

  private currentIndex : number;

  constructor() {

  }

  slideChanged() {
      this.currentIndex = this.sliderComponent.getActiveIndex();
      console.log("Current index is" + this.currentIndex);
      this.update.next([this.images[this.currentIndex].link, this.indice]);
  }

}
