import { Component, EventEmitter, Output } from '@angular/core';

import { NavController, PopoverController, ViewController } from 'ionic-angular';

import { AboutPage } from '../../pages/about/about';
import { ContactPage } from '../../pages/contact/contact';
import { LoginPage } from '../../pages/login/login';
import { AuthData } from '../../providers/auth-data';

declare var cordova;

@Component({
  template: `
    <ion-list class="popover-page">
      <ion-item>
        <ion-label (click)="goToPage('about')">Favorites</ion-label>
      </ion-item>
      <ion-item>
        <ion-label (click)="goToPage('contact')">Other</ion-label>
      </ion-item>
      <ion-item>
        <ion-label (click)="logOut()">Logout</ion-label>
      </ion-item>
    </ion-list>
  `
})

export class PopoverPage {

  constructor(private navCtrl: NavController, public viewCtrl: ViewController, public authData: AuthData) {

  }

  logOut(){
    this.authData.logoutUser().then(() => {
      this.navCtrl.pop();
      this.navCtrl.push(LoginPage);
    });
  }

  goToPage(page) {

    if(page == 'about'){

      this.navCtrl.push(AboutPage);

    }else if(page == 'contact'){

      this.navCtrl.push(ContactPage);
    }

    this.viewCtrl.dismiss();
  }
}


@Component({
  selector: 'menu',
  templateUrl: 'menu.html'
})
export class MenuComponent {

  @Output() onUpdate = new EventEmitter();

  constructor(private popoverCtrl: PopoverController) {

  }

  enabMod(){
    this.onUpdate.emit("clicked");
  }

  presentPopover(ev) {

    let popover = this.popoverCtrl.create(PopoverPage, {

    });

    popover.present({
      ev: ev
    });
  }
}
