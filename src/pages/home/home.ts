import { Component, ViewChild} from '@angular/core';
import { NavController, reorderArray, Platform, Content } from 'ionic-angular';
import { Transfer, SocialSharing} from 'ionic-native';

declare var Media;
declare var cordova;

interface M {
  url: string;
  contenturl:string
}
interface imgn {
    title: string; image: Array<M>;
}

interface imgn extends Array<imgn>{}

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  displayText : string;
  private filePath : string;
  private media : any;
  public slides : Array<any>;
  public fileImage : string;
  private platform : Platform
  private extension : string;
  private OrderShare: Array<string>;
  public modificar: boolean;
  public textmode: boolean;
  public searchtext: string;
  @ViewChild('textInput') myInput;
  @ViewChild(Content) content: Content;

  constructor(public navCtrl: NavController, platform: Platform) {
    this.searchtext = "";
    this.textmode = false;
    this.modificar = false;
    this.OrderShare = [];
    this.slides = [];
    this.displayText = '';
    platform.ready().then(() => {
    this.platform = platform;
    });

  }

  editSlide(){
    this.modificar = !this.modificar;
  }

  //Borrar items
  deleteImg(img){

    let index = this.slides.indexOf(img);

    if(index > -1){
        this.slides.splice(index, 1);
    }
 }

 handleImagesUpdated(evento:any){
   this.OrderShare[evento[1]] = evento[0];
 }

  //Drag and Drop para cambiar orden
  reorderItems(indexes){
    this.slides = reorderArray(this.slides, indexes);
    this.OrderShare = reorderArray(this.OrderShare, indexes);
  }

  startRecording(){

    this.displayText = 'grabando';
    this.filePath = "audioFile";
    var where = "";
    if (this.platform.is('ios'))
    {
        this.extension = ".wav";
        where = "/"+this.filePath + this.extension;
    }
    else if (this.platform.is('android'))
    {
        this.extension = ".amr";
        where = cordova.file.externalRootDirectory + this.filePath + this.extension;
    }

      // This will only print when on iOS
      console.log("I'm an iOS device!");
      //Constructor Media File
      this.media = new Media( where ,
      // success callback
      function(e) {
          console.log(e,"recordAudio():Audio Success");
        //  console.log(data);
      },
      // error callback
      function(e) {
          console.log(e.code,"recordAudio():Audio Error: ");
          //console.log(err);
      });

      // Start Recording
      this.media.startRecord();

  }

  otherShare(){
     console.log(this.OrderShare);
     SocialSharing.share("Genral Share Sheet",null/*Subject*/,this.OrderShare,"http://xpikt.com")
     .then(()=>{
         alert("Compartido");
       },
       ()=>{
          alert("failed")
       })
     }

  stopRecording(){
    this.displayText = '';
    this.content.scrollToBottom();

    //Timeout en espera de archivo
    setTimeout(() => {
      console.log("Stop");
      this.media.stopRecord();
      this.media.play();
      /*if (this.platform.is('android')) {
        this.media.release();
      }*/
    }, 100);
    setTimeout(() => {
      console.log("Upload");
      this.upload();
    }, 500);

  }

  private upload(){
    const fileTransfer = new Transfer();
    let options: Object;

    if (this.platform.is('ios')) {

      options = {
         fileKey: 'audioFile',
         fileName: 'audioFile.wav',
         httpMethod: 'POST',
         headers: {}
      }
      var where = cordova.file.tempDirectory+this.filePath + this.extension;
    }else if (this.platform.is('android')) {

      options = {
         fileKey: 'audioFile',
         fileName: 'audioFile.amr',
         httpMethod: 'POST',
         headers: {}
      }

      var where = cordova.file.externalRootDirectory+this.filePath + this.extension;
    }

      fileTransfer.upload(where, "http://104.196.207.201:8080/upload", options)
       .then((data:any) => {
         let respuesta = JSON.parse(data.response);
         if(respuesta.message){
            console.log("No Query");
         }else{
           var arreglo = [];
           arreglo = JSON.parse(data.response);
           this.slides.push(arreglo);
           this.OrderShare.push(arreglo[0].link);
           setTimeout(() => {
              this.content.scrollToBottom();
            },200);   
         }

       }, (err) => {
         console.error(err);
       })
  }

  changeMode(){
    this.textmode = !this.textmode;
    setTimeout(() => {
       this.myInput.setFocus();
     },200);
  }

  onCancelSearchbar(){
    this.textmode = false;
  }
}
