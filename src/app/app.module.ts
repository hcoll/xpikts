import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';
import { ResetPasswordPage } from '../pages/reset-password/reset-password';
import { PopoverPage, MenuComponent } from '../components/menu/menu';
import { SlideComp } from '../components/slide-comp/slide-comp';
import { AuthData } from '../providers/auth-data';

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    SignupPage,
    ResetPasswordPage,
    LoginPage,
    MenuComponent,
    PopoverPage,
    SlideComp
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    SignupPage,
    ResetPasswordPage,
    LoginPage,
    PopoverPage
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA],
  //providers: [{provide: ErrorHandler, useClass: IonicErrorHandler}],
  providers: [AuthData]
})
export class AppModule {}
