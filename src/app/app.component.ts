import { Component, NgZone } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';

import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';

import firebase from 'firebase';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  rootPage;

  private zone: NgZone;

  constructor(platform: Platform) {

    firebase.initializeApp({
      apiKey: "AIzaSyDl-QPWLTLTc0gEJH-QXUQtHRXBoqv-9QQ",
      authDomain: "xpikt-28611.firebaseapp.com",
      databaseURL: "https://xpikt-28611.firebaseio.com",
      storageBucket: "xpikt-28611.appspot.com",
      messagingSenderId: "187788258536"
    });

    this.zone = new NgZone({});
      const unsubscribe = firebase.auth().onAuthStateChanged((user) => {
        this.zone.run( () => {
          if (!user) {
            this.rootPage = LoginPage;
            unsubscribe();
          } else {
            this.rootPage = HomePage;
            unsubscribe();
          }
        });
      });

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
      Splashscreen.hide();
    });
  }
}
